info face="Source Sans Pro" size=100 bold=1 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=2,2
common lineHeight=126 base=98 scaleW=256 scaleH=256 pages=1 packed=0
page id=0 file="SansPro_Black_40.png"
chars count=11
char id=32     x=99    y=142   width=0     height=0     xoffset=0     yoffset=99    xadvance=21    page=0 chnl=0 letter="space"
char id=48     x=2     y=2     width=50    height=68    xoffset=3     yoffset=32    xadvance=54    page=0 chnl=0 letter="0"
char id=49     x=52    y=142   width=45    height=66    xoffset=7     yoffset=33    xadvance=54    page=0 chnl=0 letter="1"
char id=50     x=52    y=72    width=49    height=67    xoffset=3     yoffset=32    xadvance=54    page=0 chnl=0 letter="2"
char id=51     x=54    y=2     width=50    height=68    xoffset=2     yoffset=32    xadvance=54    page=0 chnl=0 letter="3"
char id=52     x=154   y=72    width=52    height=66    xoffset=2     yoffset=33    xadvance=54    page=0 chnl=0 letter="4"
char id=53     x=103   y=72    width=49    height=67    xoffset=2     yoffset=33    xadvance=54    page=0 chnl=0 letter="5"
char id=54     x=106   y=2     width=49    height=68    xoffset=4     yoffset=32    xadvance=54    page=0 chnl=0 letter="6"
char id=55     x=2     y=142   width=48    height=66    xoffset=4     yoffset=33    xadvance=54    page=0 chnl=0 letter="7"
char id=56     x=2     y=72    width=48    height=68    xoffset=4     yoffset=32    xadvance=54    page=0 chnl=0 letter="8"
char id=57     x=157   y=2     width=49    height=68    xoffset=3     yoffset=32    xadvance=54    page=0 chnl=0 letter="9"
