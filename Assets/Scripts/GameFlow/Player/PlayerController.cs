﻿using UnityEngine;
using Level.Controllers;
using Level.Initializers;


namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        #region Fields

        const float SPEED = 0.8F;

        float width;
        Vector2 startMovingPosition;
        Vector2 startPosition;

        #endregion


        #region Unity lifecycle

        void Start()
        {
            startPosition = transform.position;
        }


        void OnEnable()
        {
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void FixedUpdate()
        {
            if (LevelInitializer.LevelManagerInstance.GameIsEnabled)
            {
                if (width != 0)
                {
                    Vector2 target = new Vector2(startMovingPosition.x + width, transform.position.y);
                    transform.position = Vector2.MoveTowards(transform.position, target, SPEED);
                }
            }
        }


        void OnDisable()
        {
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Public methods

        public void SetStartPosition()
        {
            startMovingPosition = transform.position;
        }


        public void Move(float width)
        {
            this.width = width;
        }

        #endregion


        #region Event handlers

        void LevelManager_OnReset()
        {
            transform.position = startPosition;
        }

        #endregion
    }
}
