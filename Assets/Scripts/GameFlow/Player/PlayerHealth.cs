﻿using UnityEngine;
using Level.Controllers;
using Level.Block;
using Level.Initializers;


namespace Player
{
    public class PlayerHealth : MonoBehaviour
    {
        #region Properties

        public bool IsAlive { get; private set; }
        public bool IsGodMode { get; set; }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            IsAlive = true;
        }


        void OnEnable()
        {
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void OnDisable()
        {
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Event handlers

        void OnTriggerEnter2D(Collider2D collision)
        {
            if (IsAlive)
            {
                if (!IsGodMode)
                {
                    Block block = collision.GetComponent<Block>();

                    if (block != null)
                    {
                        IsAlive = false;
                        LevelInitializer.LevelManagerInstance.Finish();
                        block.Blink();
                    }
                }
            }
        }


        void LevelManager_OnReset()
        {
            IsAlive = true;
            IsGodMode = false;
        }

        #endregion
    }
}
