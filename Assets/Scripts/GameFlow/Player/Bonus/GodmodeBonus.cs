﻿using System;
using UnityEngine;

namespace Player.Bonus
{
    public class 
	GodmodeBonus : BonusBase
    {
        #region Fields

        PlayerHealth playerHealth;
        BonusType bonusType;
        Color startColor;
        tk2dSprite sprite;

        #endregion


        #region IBonus interface implementation

        public override void Apply()
		{
			lifeTime = BonusConstans.GODMODE_TIME;
			playerHealth.IsGodMode = true;
			sprite.color = BonusConstans.GODMODE_COLOR;
		}


		public override void OnExpireBonus()
		{
			playerHealth.IsGodMode = false;
			sprite.color = startColor;

			base.OnExpireBonus();
		}

		#endregion


        #region Constructor

        public GodmodeBonus(BonusType bonusType, PlayerHealth playerHealth, Action<BonusType> OnExpireBonusEvent) : base(bonusType, OnExpireBonusEvent)
        {
            this.playerHealth = playerHealth;

            sprite = playerHealth.GetComponentInChildren<tk2dSprite>();
            startColor = sprite.color;
        }

        #endregion
    }
}
