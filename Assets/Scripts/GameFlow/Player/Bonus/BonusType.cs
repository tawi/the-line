﻿namespace Player.Bonus
{
    #region BonusType enum

    public enum BonusType { scale, godmode }

    #endregion
}