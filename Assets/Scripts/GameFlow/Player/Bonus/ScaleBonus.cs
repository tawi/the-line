﻿using UnityEngine;
using System;


namespace Player.Bonus
{
    public class ScaleBonus : BonusBase
    {
        #region Fields

        TweenScale scale;
        Vector2 startScale;

        #endregion


        #region IBonus interface implementation

        public override void Apply()
		{
			lifeTime = BonusConstans.MAX_SCALE_TIME;
			scale.Play(true);
		}


		public override void OnExpireBonus()
		{
			scale.Play(false);

			base.OnExpireBonus();
		}

		#endregion


        #region Constuctor

        public ScaleBonus(BonusType bonusType, Vector2 startScale, TweenScale scale, Action<BonusType> OnExpireBonusEvent) : base(bonusType, OnExpireBonusEvent)
        {
            this.startScale = startScale;
            this.scale = scale;

            scale.Play(false);
            scale.duration = BonusConstans.SCALE_DURATION;
            scale.BeginScale = startScale;
            scale.EndScale = startScale * BonusConstans.SCALE_RATE;
        }

        #endregion
    }
}
