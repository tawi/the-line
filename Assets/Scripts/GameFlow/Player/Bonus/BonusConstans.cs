﻿using UnityEngine;

namespace Player.Bonus
{
    public static class BonusConstans
    {
        #region Fields

        public const float MAX_SCALE_TIME = 5f;
        public const float SCALE_RATE = 0.5f;
        public const float SCALE_DURATION = 0.4f;
        public const float GODMODE_TIME = 3f;
        public static readonly Color GODMODE_COLOR = Color.green;

        #endregion
    }
}