﻿using System.Collections.Generic;
using UnityEngine;
using Level.Controllers;


namespace Player.Bonus
{
    public class PlayerBonusHandler : MonoBehaviour
    {
        #region Fields

        Dictionary<BonusType, IBonus> availableBonuses = new Dictionary<BonusType, IBonus>();
        List<IBonus> currentBonuses = new List<IBonus>();

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            availableBonuses.Add(BonusType.godmode, new GodmodeBonus(BonusType.godmode, GetComponent<PlayerHealth>(), IBonus_OnExpire));
            availableBonuses.Add(BonusType.scale, new ScaleBonus(BonusType.scale, transform.localScale, GetComponent<TweenScale>(), IBonus_OnExpire));
        }


        void OnEnable()
        {
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void FixedUpdate()
        {
            for (int i = 0; i < currentBonuses.Count; i++)
            {
                currentBonuses[i].Update();
            }
        }


        void OnDisable()
        {
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Public methods

        public void Apply(BonusType type)
        {
            if (availableBonuses.ContainsKey(type))
            {
                IBonus bonus = availableBonuses[type];

                if (!currentBonuses.Contains(bonus))
                {
                    currentBonuses.Add(bonus);
                }

                bonus.Apply();
            }
            else
            {
                CustomDebug.LogError("LevelBonus doesn't exist");
            }
        }

        #endregion


        #region Event handlers

        void IBonus_OnExpire(BonusType type)
        {
            IBonus bonus = availableBonuses[type];

            if (currentBonuses.Contains(bonus))
            {
                currentBonuses.Remove(bonus);
            }
        }


        void LevelManager_OnReset()
        {
            for (int i = 0; i < currentBonuses.Count; i++)
            {
                currentBonuses[i].OnExpireBonus();
            }
        }

        #endregion
    }
}
