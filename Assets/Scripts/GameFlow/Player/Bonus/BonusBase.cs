﻿using UnityEngine;
using System;


namespace Player.Bonus
{
    public abstract class BonusBase : IBonus
    {
        #region Fields

        event Action<BonusType> OnExpireBonusEvent;

        BonusType bonusType;
        protected float lifeTime;

        #endregion


		#region IBonus interface implementation

		public void Update()
		{
			if (lifeTime > 0)
			{
				lifeTime -= Time.fixedDeltaTime;

				if (lifeTime <= 0)
				{
					OnExpireBonus();
				}
			}
		}


		public abstract void Apply();


		public virtual void OnExpireBonus()
		{
			OnExpireBonusEvent(bonusType);
		}

		#endregion


        #region Constructor

        protected BonusBase(BonusType bonusType, Action<BonusType> OnExpireBonusEvent)
        {
            this.bonusType = bonusType;
            this.OnExpireBonusEvent = OnExpireBonusEvent;
        }

        #endregion
    }
}
