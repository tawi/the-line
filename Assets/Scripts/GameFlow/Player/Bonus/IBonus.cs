﻿namespace Player.Bonus
{
	#region IBonus interface

	public interface IBonus
	{
		void Apply();
		void Update();
		void OnExpireBonus();
	}

	#endregion
}