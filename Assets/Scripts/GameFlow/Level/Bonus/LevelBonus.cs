﻿using UnityEngine;
using Player.Bonus;
using Level.Initializers;


namespace Level.Bonus
{
    public class LevelBonus : MonoBehaviour
    {
        #region Fields

        const float START_LIFE_TIME = 8F;

        [SerializeField] BonusType bonusType;

        float currentLifeTime;
        ActivatedBonusPool activatedBonusPool;
        PoolableObjectInfo poolableObjectInfo;

        #endregion


        #region Properties

        public BonusType Type
        {
            get
            {
                return bonusType;
            }
        }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            poolableObjectInfo = GetComponent<PoolableObjectInfo>();
            activatedBonusPool = BonusInitializer.ActivatedBonusPoolInstance;
        }


        void OnEnable()
        {
            currentLifeTime = START_LIFE_TIME;
            activatedBonusPool.AddBonus(poolableObjectInfo);
        }


        void FixedUpdate()
        {
            if (LevelInitializer.LevelManagerInstance.GameIsEnabled)
            {
                currentLifeTime -= Time.fixedDeltaTime;

                if (currentLifeTime <= 0)
                {
                    activatedBonusPool.RemoveBonus(poolableObjectInfo);
                }
            }
        }

        #endregion


        #region Event handlers

        void OnTriggerEnter2D(Collider2D collision)
        {
            PlayerBonusHandler playerBonusHandler = collision.GetComponent<PlayerBonusHandler>();

            if (playerBonusHandler != null)
            {
                playerBonusHandler.Apply(bonusType);
                activatedBonusPool.RemoveBonus(poolableObjectInfo);
            }
        }

        #endregion
    }
}
