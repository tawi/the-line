﻿using UnityEngine;
using System.Collections.Generic;


namespace Level.Bonus
{
    public class BonusSpawner : MonoBehaviour
    {
        #region Fields

        const float SPAWN_CHANCE = 0.1F;

        List<ObjectPool> pools = new List<ObjectPool>();

        #endregion


        #region Properties

        bool CanSpawn
        {
            get
            {
                float chance = Random.Range(0f, 1f);
                return chance <= SPAWN_CHANCE;
            }
        }

        #endregion


        #region Public methods

        public void InitializePool(List<ObjectPool> pools)
        {
            this.pools = pools;
        }


        public void Spawn(Vector2 spawnPosition)
        {
            if (CanSpawn)
            {
                int poolIndex = Random.Range(0, pools.Count);

                GameObject levelBonus = pools[poolIndex].Pop();

                levelBonus.transform.position = spawnPosition;
            }
        }

        #endregion
    }
}
