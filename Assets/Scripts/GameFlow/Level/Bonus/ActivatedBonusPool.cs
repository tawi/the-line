﻿using System.Collections.Generic;
using UnityEngine;
using Level.Controllers;


namespace Level.Bonus
{
    public class ActivatedBonusPool : MonoBehaviour
    {
        #region Fields

        List<PoolableObjectInfo> activatedBonuses = new List<PoolableObjectInfo>();

        #endregion


        #region Unity lifecycle

        void OnEnable()
        {
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void OnDisable()
        {
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Public methods

        public void AddBonus(PoolableObjectInfo poolableObjectInfo)
        {
            if (!activatedBonuses.Contains(poolableObjectInfo))
            {
                activatedBonuses.Add(poolableObjectInfo);
            }
        }


        public void RemoveBonus(PoolableObjectInfo poolableObjectInfo)
        {
            if (activatedBonuses.Contains(poolableObjectInfo))
            {
                activatedBonuses.Remove(poolableObjectInfo);
                poolableObjectInfo.ReturnToPool();
            }
        }

        #endregion


        #region Event handlers

        void LevelManager_OnReset()
        {
            for (int i = 0; i < activatedBonuses.Count; i++)
            {
                activatedBonuses[i].ReturnToPool();
            }

            activatedBonuses.Clear();
        }

        #endregion
    }
}
