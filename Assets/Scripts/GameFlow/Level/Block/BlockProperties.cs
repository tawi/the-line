﻿using UnityEngine;


namespace Level.Block
{
    public class BlockProperties : MonoBehaviour
    {
        #region Properties

        public int VerticalIndex { get; set; }
        public int HoritzontalIndex { get; set; }

        #endregion
    }
}
