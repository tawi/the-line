﻿using System.Collections.Generic;
using UnityEngine;
using Level.Controllers;
using Level.Initializers;


namespace Level.Block
{
    public class LevelBlockSwitcher : MonoBehaviour
    {
        #region Fields

        const float MIN_Y_POSITION = -14F;
        
        Vector3 startPosition;
        float min_y;

        Queue<List<GameObject>> blocks = new Queue<List<GameObject>>();
        Transform loadPoint;
        LevelGenerator levelGenerator;
        LevelLayout levelLayout;

        #endregion


        #region Properties

        Vector3 NextLoadPosition
        {
            get
            {
                return levelLayout.BottomMiddlePosition;
            }
        }

        #endregion


        #region Unity lifecycle

        void OnEnable()
        {
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void FixedUpdate()
        {
            if (ShouldSwitchLine())
            {
                NextLine();
            }
        }


        void OnDisable()
        {
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Public methods

        public void Initialize(tk2dCamera gameCamera)
        {
            levelGenerator = LevelInitializer.LevelGeneratorInstance;
            levelLayout = LevelInitializer.LevelLayoutInstance;

            loadPoint = GameInitializer.LevelMoverInstance.LoadPoint;
            startPosition = loadPoint.position;

            Vector3 positionAfterNextLine = gameCamera.ScreenCamera.ViewportToWorldPoint(new Vector2(0.5f, 0));

            min_y = (positionAfterNextLine - new Vector3(0, levelLayout.VerticalBlockShift)).y;

            InitializeLoadPoint(gameCamera);
        }


        public void AddLine(List<GameObject> newLine)
        {
            blocks.Enqueue(newLine);
        }


        #endregion


        #region Private methods

        void InitializeLoadPoint(tk2dCamera gameCamera)
        {
            Vector3 shiftRelativeLeftTopCamera = new Vector3(0, LevelGenerator.START_VISIBLE_BLOCKS * levelLayout.VerticalBlockShift);

            Vector3 topCameraCorner = gameCamera.ScreenCamera.ViewportToWorldPoint(new Vector2(0.5f, 1));
            Vector3 loadPointPosition = topCameraCorner - shiftRelativeLeftTopCamera + new Vector3(0, levelLayout.VerticalBlockShift) * 2;
            loadPoint.position = loadPointPosition;
            startPosition = loadPoint.position;
        }


        private bool ShouldSwitchLine()
        {
            return loadPoint.position.y <= min_y;
        }


        void NextLine()
        {
            List<GameObject> oldLine = blocks.Dequeue();

            for (int i = 0; i < oldLine.Count; i++)
            {
                oldLine[i].GetComponent<PoolableObjectInfo>().ReturnToPool();
            }

            levelGenerator.NextLine();
            loadPoint.position = NextLoadPosition;
        }

        #endregion


        #region Event handlers

        void LevelManager_OnReset()
        {
            loadPoint.position = startPosition;

            while (blocks.Count > 0)
            {
                List<GameObject> oldLine = blocks.Dequeue();

                for (int i = 0; i < oldLine.Count; i++)
                {
                    oldLine[i].GetComponent<PoolableObjectInfo>().ReturnToPool();
                }
            }

            levelGenerator.Reset();
        }        
       
        #endregion
    }
}
