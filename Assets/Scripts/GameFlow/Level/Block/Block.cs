﻿using UnityEngine;


namespace Level.Block
{
    public class Block : MonoBehaviour
    {
        #region Fields

        TweenColor tweenColor;

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            tweenColor = GetComponent<TweenColor>();
            tweenColor.beginColor = GetComponent<tk2dSprite>().color;
        }


        void OnEnable()
        {
            tweenColor.SetBeginStateImmediately();
            tweenColor.ResetTween();
        }


        void OnDisable()
        {
            tweenColor.enabled = false;
        }

        #endregion


        #region Public methods

        public void Blink()
        {
            tweenColor.Play(true);
        }

        #endregion
    }
}
