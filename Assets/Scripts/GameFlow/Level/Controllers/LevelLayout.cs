﻿using UnityEngine;
using Level.Block;
using Level.Initializers;


namespace Level.Controllers
{
    public class LevelLayout : MonoBehaviour
    {
        #region Fields

        int middleIndex;

        #endregion


        #region Properties

        public Vector3 GridStartPosition { get; private set; }
        public Vector3 PoolStartPosition { get; private set; }
        public float VerticalBlockShift { get; private set; }
        public float HorizontalBlockShift { get; private set; }


        public Vector3 BottomMiddlePosition
        {
            get
            {
                Vector3 middlePosition = GridStartPosition + new Vector3(middleIndex * HorizontalBlockShift, LevelInitializer.LevelGeneratorInstance.BottomLineIndex * VerticalBlockShift) - PoolShiftFromStart;

                return middlePosition;
            }
        }


        public Vector3 PlayerSpawnPosition
        {
            get
            {
                Vector3 middlePosition = GridStartPosition + new Vector3(middleIndex * HorizontalBlockShift, 0) - PoolShiftFromStart;

                return middlePosition;
            }
        }


        Vector3 PoolShiftFromStart
        {
            get
            {
                return PoolStartPosition - GameInitializer.LevelMoverInstance.transform.position;
            }
        }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            PoolStartPosition = GameInitializer.LevelMoverInstance.transform.position;
            middleIndex = Mathf.FloorToInt((float)LevelGenerator.MAX_HORIZONTAL_COUNT_BLOCK / 2);
        }

        #endregion


        #region Public methods

        public void InitializeGridStartPosition(tk2dCamera gameCamera)
        {
            Vector3 leftTopCameraCorner = gameCamera.ScreenCamera.ViewportToWorldPoint(new Vector2(0, 1));
            leftTopCameraCorner.z = 0;
            Vector3 shiftRelativeLeftTopCamera = new Vector3(0, LevelGenerator.START_VISIBLE_BLOCKS * VerticalBlockShift);
            Vector3 halfWidthBlock = new Vector3(HorizontalBlockShift / 2, 0);
            Vector3 halfHeightBlock = new Vector3(0, VerticalBlockShift / 2);

            GridStartPosition = leftTopCameraCorner - shiftRelativeLeftTopCamera + halfWidthBlock + halfHeightBlock;
        }


        public void InitializeBlockSize(GameObject blockPrefab)
        {
            HorizontalBlockShift = blockPrefab.GetComponent<tk2dSprite>().GetBounds().size.x * blockPrefab.transform.localScale.x;
            VerticalBlockShift = blockPrefab.GetComponent<tk2dSprite>().GetBounds().size.y * blockPrefab.transform.localScale.y;
        }


        public Vector2 GetSpawnBonusPosition(int previousVerticalIndex, int startGap, int endGap)
        {
            float xPosition = Random.Range(startGap * HorizontalBlockShift, endGap * HorizontalBlockShift);
            Vector3 bonusSpawnPosition = new Vector3(xPosition, previousVerticalIndex * VerticalBlockShift) - PoolShiftFromStart;
            bonusSpawnPosition = GridStartPosition + bonusSpawnPosition;

            return bonusSpawnPosition;
        }


        public Vector2 GetSpawnBlockPosition(int horizontalIndex, int verticalIndex)
        {
            Vector3 blockPosition = new Vector3(horizontalIndex * HorizontalBlockShift, verticalIndex * VerticalBlockShift) - PoolShiftFromStart;
            blockPosition = GridStartPosition + blockPosition;

            return blockPosition;
        }

        #endregion
    }
}
