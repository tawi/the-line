﻿using System.Collections.Generic;
using UnityEngine;
using Level.Block;
using Level.Initializers;


namespace Level.Controllers
{
    public class LevelGenerator : MonoBehaviour
    {
        #region Fields

        public const int MAX_HORIZONTAL_COUNT_BLOCK = 7;
        public const int START_VISIBLE_BLOCKS = 5;
        const int START_RANDOM_BLOCKS = 2;
        readonly List<int> startBorderCount = new List<int>() { 1, 1, 2, 2, 3, 3, 3, 3 };

        int previousEntryIndex;
        int previousWidth = 1;
        int previousVerticalIndex;
        int bottomLineIndex = -2;
        int middleIndex;
        int additionalLines;

        ObjectPool objectPool;

        #endregion


        #region Properties

        public int BottomLineIndex
        {
            get
            {
                return bottomLineIndex;
            }
        }


        GameObject PooledObject
        {
            get
            {
                return objectPool.Pop();
            }
        }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            bottomLineIndex = START_VISIBLE_BLOCKS - startBorderCount.Count + 1;
            middleIndex = Mathf.FloorToInt((float)MAX_HORIZONTAL_COUNT_BLOCK / 2);
            previousEntryIndex = middleIndex;
            previousVerticalIndex = startBorderCount.Count - 1;
            additionalLines = MAX_HORIZONTAL_COUNT_BLOCK - 7;
        }

        #endregion


        #region Public methods

        public void InitializePool(ObjectPool blockPool)
        {
            objectPool = blockPool;
        }


        public void GenerateStartMap()
        {
            for (int i = 0; i < startBorderCount.Count; i++)
            {
                List<GameObject> newLine = new List<GameObject>();

                for (int j = 0; j < startBorderCount[i]; j++)
                {
                    int leftHorizonalEdgeIndex = j;
                    int rightHorizonalEdgeIndex = MAX_HORIZONTAL_COUNT_BLOCK - j - 1;

                    GameObject leftBlock = PooledObject;
                    InitBlock(leftBlock, leftHorizonalEdgeIndex, i);

                    GameObject rightBlock = PooledObject;
                    InitBlock(rightBlock, rightHorizonalEdgeIndex, i);

                    newLine.Add(leftBlock);
                    newLine.Add(rightBlock);
                }

                LevelInitializer.LevelBlockSwitcherInstance.AddLine(newLine);
            }

            for (int i = 0; i < START_RANDOM_BLOCKS + additionalLines; i++)
            {
                NextLine();
            }
        }


        public void NextLine()
        {
            bottomLineIndex++;
            previousVerticalIndex++;

            int side = Random.Range(0, 2);
            side = side == 0 ? -1 : side;

            int nextIndex = GetNextIndex(side);

            int startGap;
            int endGap;

            if (side == -1)
            {
                startGap = nextIndex;
                endGap = previousEntryIndex;
            }
            else
            {
                startGap = previousEntryIndex;
                endGap = nextIndex;
            }

            FillLine(startGap, endGap, previousVerticalIndex);

            previousEntryIndex = nextIndex;

            SpawnBonus(startGap, endGap);
        }


        public void Reset()
        {
            Awake();
            GenerateStartMap();
        }

        #endregion


        #region Private methods

        int GetNextIndex(int side)
        {
            int index = previousEntryIndex;

            if (previousWidth == 1)
            {
                if (side == -1)
                {
                    index = Random.Range(1, previousEntryIndex);
                    previousWidth = previousEntryIndex - index + 1;
                }
                else
                {
                    index = Random.Range(previousEntryIndex, MAX_HORIZONTAL_COUNT_BLOCK - 1);
                    previousWidth = index - previousEntryIndex + 1;
                }
            }
            else
            {
                previousWidth = 1;
            }

            return index;
        }


        void FillLine(int startGap, int endGap, int lineIndex)
        {
            List<GameObject> newLine = new List<GameObject>();

            for (int i = 0; i < MAX_HORIZONTAL_COUNT_BLOCK; i++)
            {
                if (i < startGap || i > endGap)
                {
                    GameObject block = PooledObject;
                    InitBlock(block, i, lineIndex);

                    newLine.Add(block);
                }
            }

            LevelInitializer.LevelBlockSwitcherInstance.AddLine(newLine);
        }


        void SpawnBonus(int startGap, int endGap)
        {
            Vector3 bonusSpawnPosition = LevelInitializer.LevelLayoutInstance.GetSpawnBonusPosition(previousVerticalIndex, startGap, endGap);
            BonusInitializer.BonusSpawnerInstance.Spawn(bonusSpawnPosition);
        }


        void InitBlock(GameObject block, int horizontalIndex, int verticalIndex)
        {
            BlockProperties properties = block.GetComponent<BlockProperties>();
            properties.HoritzontalIndex = horizontalIndex;
            properties.VerticalIndex = verticalIndex;
            SetBlockPosition(block.transform, properties);
        }


        void SetBlockPosition(Transform block, BlockProperties properties)
        {
            Vector3 blockPosition = LevelInitializer.LevelLayoutInstance.GetSpawnBlockPosition(properties.HoritzontalIndex, properties.VerticalIndex);
            block.transform.position = blockPosition;
        }

        #endregion
    }
}
