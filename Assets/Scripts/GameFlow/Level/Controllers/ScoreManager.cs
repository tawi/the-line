﻿using UnityEngine;
using System;
using Level.Initializers;


namespace Level.Controllers
{
    public class ScoreManager : MonoBehaviour
    {
        #region Fields

        const string HIGHSCORE_KEY = "HighScore";
        const float SCORE_PER_FRAME = 0.1f;

        public static event Action<int> OnScoreChanged = delegate { };

        float scoreFloat;

        #endregion


        #region Properties

        public int Score { get; private set; }
        public int HighScore { get; private set; }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            if (PlayerPrefs.HasKey(HIGHSCORE_KEY))
            {
                HighScore = PlayerPrefs.GetInt(HIGHSCORE_KEY);
            }
        }


        void OnEnable()
        {
            LevelManager.OnFinish += LevelManager_OnFinish;
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void FixedUpdate()
        {
            if (LevelInitializer.LevelManagerInstance != null && LevelInitializer.LevelManagerInstance.GameIsEnabled)
            {
                scoreFloat += SCORE_PER_FRAME;

                int flooredScore = Mathf.FloorToInt(scoreFloat);

                if (flooredScore > Score)
                {
                    ChangeScore();
                }
            }
        }


        void OnDisable()
        {
            LevelManager.OnFinish -= LevelManager_OnFinish;
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Private methods

        void ChangeScore()
        {
            Score = Mathf.FloorToInt(scoreFloat);
            OnScoreChanged(Score);
        }


        void SaveHighScore()
        {
            PlayerPrefs.SetInt(HIGHSCORE_KEY, HighScore);
            PlayerPrefs.Save();
        }

        #endregion


        #region Event handlers

        void LevelManager_OnFinish()
        {
            if (Score > HighScore)
            {
                HighScore = Score;
                SaveHighScore();
            }
        }


        void LevelManager_OnReset()
        {
            Score = 0;
            scoreFloat = 0;
            OnScoreChanged(Score);
        }

        #endregion

    }
}