﻿using UnityEngine;


namespace Level.Controllers
{
    public class LevelMover : MonoBehaviour
    {
        #region Fields

        const float TWEENER_SHIFT = -50F;

        [SerializeField] Transform loadPoint;
        [SerializeField] TweenPosition tweenPosition;

        Vector3 startPosition;

        #endregion


        #region Properties

        public Transform LoadPoint
        {
            get
            {
                return loadPoint;
            }
        }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            startPosition = transform.position;

            LevelManager_OnReset();
        }


        void OnEnable()
        {
            tweenPosition.SetOnFinishedDelegate(TweenPosition_OnTargetAchieved);
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void OnDisable()
        {
            tweenPosition.RemoveOnFinishedDelegate(TweenPosition_OnTargetAchieved);
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Public methods

        public void EnableMover()
        {
            if (!tweenPosition.enabled)
            {
                ResetTweener();
            }
        }


        public void DisableMover()
        {
            tweenPosition.enabled = false;
        }

        #endregion


        #region Event handlers

        void TweenPosition_OnTargetAchieved(ITweener tweener)
        {
            tweenPosition.beginPosition = transform.position;
            tweenPosition.endPosition = transform.position + new Vector3(0, TWEENER_SHIFT);
            ResetTweener();
        }


        void LevelManager_OnReset()
        {
            transform.position = startPosition;
            tweenPosition.beginPosition = startPosition;
            tweenPosition.endPosition = startPosition + new Vector3(0, TWEENER_SHIFT);
            DisableMover();
        }


        void ResetTweener()
        {
            tweenPosition.ResetTween();
            tweenPosition.SetBeginStateImmediately();
            tweenPosition.Play(true);
        }

        #endregion
    }
}
