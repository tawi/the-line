﻿using UnityEngine;
using System;
using Player;
using Level.Initializers;


namespace Level.Controllers
{
    public class LevelManager : MonoBehaviour
    {
        #region Fields

        public static event Action OnFinish = delegate { };
        public static event Action OnReset = delegate { };

        PlayerHealth playerHealth;

        #endregion


        #region Properties

        public bool GameIsEnabled { get; private set; }

        #endregion


        #region Unity lifecycle

        void Start()
        {
            playerHealth = LevelInitializer.PlayerInstance.GetComponent<PlayerHealth>();
            GenerateMap();
        }

        #endregion


        #region Public methods

        public void EnableGame()
        {
            if (playerHealth.IsAlive)
            {
                GameIsEnabled = true;
                GameInitializer.LevelMoverInstance.EnableMover();
            }
        }


        public void Finish()
        {
            GameIsEnabled = false;
            GameInitializer.LevelMoverInstance.DisableMover();
            OnFinish();
        }


        public void Reset()
        {
            GameIsEnabled = false;
            OnReset();
        }

        #endregion


        #region Private methods

        void GenerateMap()
        {
            LevelInitializer.LevelGeneratorInstance.GenerateStartMap();
        }

        #endregion
    }
}
