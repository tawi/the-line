﻿using UnityEngine;
using UI;
using UI.Control;


namespace Level.Initializers
{
    public class UIInitializer : MonoBehaviour
    {
        #region Fields

        [SerializeField] GameObject guiPrefab;
        [SerializeField] GameScreen gameScreenPrefab;
        [SerializeField] MenuScreen menuScreenPrefab;
        [SerializeField] GameOverScreen gaveOverPrefab;

        Transform guiParent;

        #endregion


        #region Properties

        public static MenuScreen MenuScreenInstance { get; private set; }
        public static GameOverScreen GameOverScreenInstance { get; private set; }
        public static GameScreen GameScreenInstance { get; private set; }

        #endregion


        #region Unity lifecycle

        private void Start()
        {
            InitializeGUIParent();
            InitializeGameScreen();
            InitializeMenuScreen();
            InitializeGameOverScreen();
            InitializerControl();
            Destroy(gameObject);
        }

        #endregion


        #region Private methods

        void InitializeGUIParent()
        {
            guiParent = Instantiate(guiPrefab).transform;
        }

        void InitializeGameScreen()
        {
            GameScreenInstance = Instantiate(gameScreenPrefab, guiParent).GetComponent<GameScreen>();
        }

        void InitializeMenuScreen()
        {
            MenuScreenInstance = Instantiate(menuScreenPrefab, guiParent).GetComponent<MenuScreen>();
        }

        void InitializeGameOverScreen()
        {
            GameOverScreenInstance = Instantiate(gaveOverPrefab, guiParent).GetComponent<GameOverScreen>();
        }

        void InitializerControl()
        {
            guiParent.gameObject.AddComponent<Control>();
        }

        #endregion
    }
}
