﻿using UnityEngine;


namespace Level.Initializers
{
    public class GameCameraInitializer : MonoBehaviour
    {
        #region Fields

        [SerializeField] tk2dCamera gameCamera;

        #endregion


        #region Properties

        public tk2dCamera Camera
        {
            get
            {
                return gameCamera;
            }
        }

        #endregion


        #region Public Methods

        public void InititializeCameraSize(float horizontalBlockShift, int horizontalCountBlock)
        {
            float screenRate = (float)Screen.height / (float)Screen.width / 2;

            float cameraSize = horizontalBlockShift * (horizontalCountBlock) * screenRate;
            gameCamera.CameraSettings.orthographicSize = cameraSize;
        }

        #endregion
    }
}
