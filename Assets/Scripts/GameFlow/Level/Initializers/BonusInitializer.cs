﻿using UnityEngine;
using System.Collections.Generic;
using Level.Bonus;


namespace Level.Initializers
{
    public class BonusInitializer : MonoBehaviour
    {
        #region Fields

        [SerializeField] ActivatedBonusPool activatedBonusPoolPrefab;
        [SerializeField] PoolManager bonusesPoolPrefab;
        [SerializeField] BonusSpawner bonusSpawnerPrefab;

        Transform levelParent;
        Transform controllersParent;

        #endregion


        #region Properties

        public static BonusSpawner BonusSpawnerInstance { get; private set; }
        public static ActivatedBonusPool ActivatedBonusPoolInstance { get; private set; }

        #endregion


        #region Unity lifecycle

        void Start()
        {
            CreateActivatedBonusPool();

            PoolManager bonusesPoolManager = CreateBonusPool();
            CreateBonusSpawner(bonusesPoolManager);
            Destroy(gameObject);
        }

        #endregion


        #region Public methods

        public void InitializeParents(Transform levelParent, Transform controllersParent)
        {
            this.levelParent = levelParent;
            this.controllersParent = controllersParent;
        }

        #endregion


        #region Private fields

        void CreateActivatedBonusPool()
        {
            ActivatedBonusPoolInstance = Instantiate(activatedBonusPoolPrefab.gameObject, levelParent).GetComponent<ActivatedBonusPool>();
        }


        PoolManager CreateBonusPool()
        {
            return Instantiate(bonusesPoolPrefab.gameObject, levelParent).GetComponent<PoolManager>();
        }


        void CreateBonusSpawner(PoolManager bonusesPoolManager)
        {
            BonusSpawnerInstance = Instantiate(bonusSpawnerPrefab.gameObject, controllersParent).GetComponent<BonusSpawner>();

            List<ObjectPool> bonusPools = bonusesPoolManager.pools;
            BonusSpawnerInstance.InitializePool(bonusPools);
        }

        #endregion
    }
}
