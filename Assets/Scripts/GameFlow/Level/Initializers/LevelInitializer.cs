﻿using UnityEngine;
using Level.Controllers;
using Level.Block;
using Player;


namespace Level.Initializers
{
    public class LevelInitializer : MonoBehaviour
    {
        #region Fields

        [SerializeField] PoolManager blockPoolPrefab;
        [SerializeField] LevelLayout levelLayoutPrefab;
        [SerializeField] GameCameraInitializer cameraInitializerPrefab;
        [SerializeField] LevelBlockSwitcher levelBlockSwitcherPrefab;
        [SerializeField] LevelGenerator levelGeneratorPrefab;
        [SerializeField] PlayerFactory playerFactoryPrefab;
        [SerializeField] LevelManager levelManagerPrefab;

        Transform levelParent;
        Transform controllersParent;

        #endregion


        #region Properties

        public static LevelManager LevelManagerInstance { get; private set; }
        public static LevelGenerator LevelGeneratorInstance { get; private set; }
        public static LevelLayout LevelLayoutInstance { get; private set; }
        public static LevelBlockSwitcher LevelBlockSwitcherInstance { get; private set; }
        public static PlayerController PlayerInstance { get; private set; }
        public static tk2dCamera GameCameraInstance { get; private set; }

        #endregion


        #region Unity lifecycle

        void Start()
        {
            PoolManager blockPoolManager = CreateBlockPool();

            InitializeLevelLayout(blockPoolManager);
            InitializeCamera(LevelLayoutInstance);
            InitializeLevelGenerator(blockPoolManager);
            LevelLayoutInstance.InitializeGridStartPosition(GameCameraInstance);
            InitializeBlockSwitcher(GameCameraInstance);
            InitializePlayer(LevelLayoutInstance);
            InitializeLevelManager();

            Destroy(gameObject);
        }

        #endregion


        #region Public methods

        public void InitializeParents(Transform levelParent, Transform controllersParent)
        {
            this.levelParent = levelParent;
            this.controllersParent = controllersParent;
        }

        #endregion


        #region Private methods

        PoolManager CreateBlockPool()
        {
            return Instantiate(blockPoolPrefab.gameObject, levelParent).GetComponent<PoolManager>();
        }


        void InitializeLevelLayout(PoolManager blockPoolManager)
        {
            LevelLayoutInstance = Instantiate(levelLayoutPrefab.gameObject, controllersParent).GetComponent<LevelLayout>();

            GameObject blockPrefab = blockPoolManager.pools[0].prefab;
            LevelLayoutInstance.InitializeBlockSize(blockPrefab);
        }


        void InitializeCamera(LevelLayout levelLayout)
        {
            GameCameraInitializer cameraInitializer = Instantiate(cameraInitializerPrefab.gameObject).GetComponent<GameCameraInitializer>();

            cameraInitializer.InititializeCameraSize(levelLayout.HorizontalBlockShift, LevelGenerator.MAX_HORIZONTAL_COUNT_BLOCK);

            GameCameraInstance = cameraInitializer.Camera;
        }

        void InitializeBlockSwitcher(tk2dCamera gameCamera)
        {
            LevelBlockSwitcherInstance = Instantiate(levelBlockSwitcherPrefab.gameObject, controllersParent).GetComponent<LevelBlockSwitcher>();

            LevelBlockSwitcherInstance.Initialize(gameCamera);
        }


        void InitializeLevelGenerator(PoolManager blockPoolManager)
        {
            LevelGeneratorInstance = Instantiate(levelGeneratorPrefab.gameObject, controllersParent).GetComponent<LevelGenerator>();

            ObjectPool blockPool = blockPoolManager.pools[0];
            LevelGeneratorInstance.InitializePool(blockPool);
        }


        void InitializePlayer(LevelLayout levelLayout)
        {
            PlayerFactory playerFactory = Instantiate(playerFactoryPrefab.gameObject, controllersParent).GetComponent<PlayerFactory>();

            PlayerInstance = playerFactory.Create(levelLayout.PlayerSpawnPosition);
        }


        void InitializeLevelManager()
        {
            LevelManagerInstance = Instantiate(levelManagerPrefab.gameObject, controllersParent).GetComponent<LevelManager>();
        }

        #endregion
    }
}
