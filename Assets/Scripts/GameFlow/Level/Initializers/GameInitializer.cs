﻿using UnityEngine;
using Level.Bonus;
using Level.Controllers;


namespace Level.Initializers
{
    public class GameInitializer : MonoBehaviour
    {
        #region Fields

        const string CONTROLLERS_PARENT_NAME = "Controllers";

        [SerializeField] LevelMover levelPrefab;
        [SerializeField] BonusInitializer bonusInitializerPrefab;
        [SerializeField] LevelInitializer levelInitializerPrefab;
        [SerializeField] ScoreManager scoreManagerPrefab;
        [SerializeField] UIInitializer uIInitializer;

        Transform levelParent;
        Transform controllersParent;

        #endregion


        #region Properties

        public static LevelMover LevelMoverInstance { get; private set; }
        public static ScoreManager ScoreManagerInstance { get; private set; }

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            InitializeLevelParent();
            InitializeBonus();
            InitializeLevel();
            InitializeScoreManager();
            InitializeUI();
            Destroy(gameObject);
        }

        #endregion


        #region Private methods

        void InitializeLevelParent()
        {
            LevelMoverInstance = Instantiate(levelPrefab.gameObject).GetComponent<LevelMover>();
            levelParent = LevelMoverInstance.transform;
            controllersParent = new GameObject(CONTROLLERS_PARENT_NAME).transform;
        }


        void InitializeBonus()
        {
            BonusInitializer bonusInitializer = Instantiate(bonusInitializerPrefab.gameObject).GetComponent<BonusInitializer>();
            bonusInitializer.InitializeParents(levelParent, controllersParent);
        }


        void InitializeLevel()
        {
            LevelInitializer levelInitializer = Instantiate(levelInitializerPrefab.gameObject).GetComponent<LevelInitializer>();
            levelInitializer.InitializeParents(levelParent, controllersParent);
        }


        void InitializeScoreManager()
        {
            ScoreManagerInstance = Instantiate(scoreManagerPrefab.gameObject, controllersParent).GetComponent<ScoreManager>();
        }


        void InitializeUI()
        {
            Instantiate(uIInitializer.gameObject);
        }

        #endregion
    }
}
