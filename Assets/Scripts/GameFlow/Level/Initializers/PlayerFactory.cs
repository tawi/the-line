﻿using UnityEngine;
using Player;


namespace Level.Initializers
{
    public class PlayerFactory : MonoBehaviour
    {
        #region Fields

        [SerializeField] PlayerController playerPrefab;

        #endregion


        #region Public methods

        public PlayerController Create(Vector2 spawnPosition)
        {
            PlayerController playerHealth = Instantiate(playerPrefab.gameObject, spawnPosition, Quaternion.identity).GetComponent<PlayerController>();
            return playerHealth;
        }

        #endregion
    }
}
