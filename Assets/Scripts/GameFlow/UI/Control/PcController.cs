﻿using UnityEngine;


namespace UI.Control
{
    public class PcController : IController
    {
        #region IController interface implementation

        public Vector3 GetInputPosition()
        {
            return Input.mousePosition;
        }


        public bool OnBeginInput()
        {
            return Input.GetMouseButtonDown(0);
        }


        public bool OnEndInput()
        {
            return Input.GetMouseButtonUp(0);
        }

        #endregion
    }
}
