﻿using UnityEngine;


namespace UI.Control
{
	public class MobileController : IController
	{
        #region IController interface implementation

        public Vector3 GetInputPosition()
		{
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);

				return touch.position;
			}
			else
			{
				return Vector3.zero;
			}
		}


		public bool OnBeginInput()
		{
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);

				return touch.phase == TouchPhase.Began;
			}
			else
			{
				return false;
			}
		}


		public bool OnEndInput()
		{
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);

				return touch.phase == TouchPhase.Ended;
			}
			else
			{
				return false;
			}
		}

		#endregion
	}
}
