﻿using UnityEngine;
using Player;
using Level.Controllers;
using Level.Initializers;


namespace UI.Control
{
    public class Control : MonoBehaviour
    {
        #region Fields

        bool isMoving = false;
        Vector2 startPosition;
        Vector2 endPosition;

        Camera touchCamera;
        PlayerController target;
        IController controller;
        tk2dUIItem tapButton;

        #endregion


        #region Unity lifecycle

        void Awake()
        {
            touchCamera = LevelInitializer.GameCameraInstance.ScreenCamera;
            target = LevelInitializer.PlayerInstance;

            if (Application.isMobilePlatform)
            {
                controller = new MobileController();
            }
            else
            {
                controller = new PcController();
            }

            GameScreen gameScreen = GetComponentInChildren<GameScreen>();
            tapButton = gameScreen.TapButton;
        }


        void OnEnable()
        {
            tapButton.OnDown += TapButton_OnDown;
            LevelManager.OnReset += LevelManager_OnReset;
        }


        void Update()
        {
            if (isMoving)
            { 
                endPosition = touchCamera.ScreenToWorldPoint(controller.GetInputPosition());

                float width = endPosition.x - startPosition.x;
                target.Move(width);

                if (controller.OnEndInput())
                {
                    isMoving = false;
                    target.Move(0);
                }
            }
        }


        void OnDisable()
        {
            tapButton.OnDown -= TapButton_OnDown;
            LevelManager.OnReset -= LevelManager_OnReset;
        }

        #endregion


        #region Event handlers

        void TapButton_OnDown()
        {
            isMoving = true;
            LevelInitializer.LevelManagerInstance.EnableGame();
            startPosition = touchCamera.ScreenToWorldPoint(controller.GetInputPosition());
            target.SetStartPosition();
        }


        void LevelManager_OnReset()
        {
            isMoving = false;
        }

        #endregion
    }
}
