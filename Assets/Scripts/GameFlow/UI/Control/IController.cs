﻿using UnityEngine;


namespace UI.Control
{
	#region IController interface

	public interface IController
	{
		Vector3 GetInputPosition();
		bool OnBeginInput();
		bool OnEndInput();
	}

	#endregion
}