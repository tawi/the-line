﻿using System.Collections;
using UnityEngine;
using Level.Controllers;
using Level.Initializers;


namespace UI
{
    public class GameScreen : MonoBehaviour
    {
        #region Fields

        const float DELAY_TIME = 2F;

        [SerializeField] tk2dUIItem menuButton;
        [SerializeField] tk2dTextMesh scoreText;
        [SerializeField] tk2dUIItem tapButton;

        #endregion


        #region Properties

        public tk2dUIItem TapButton
        {
            get
            {
                return tapButton;
            }
        }

        #endregion


        #region Unity lifecycle

        void OnEnable()
        {
            menuButton.OnClick += MenuButton_OnClick;
            ScoreManager.OnScoreChanged += ScoreManager_OnScoreChanged;
            LevelManager.OnFinish += LevelManager_OnFinish;
        }


        void OnDisable()
        {
            menuButton.OnClick -= MenuButton_OnClick;
            ScoreManager.OnScoreChanged -= ScoreManager_OnScoreChanged;
            LevelManager.OnFinish -= LevelManager_OnFinish;
        }

        #endregion


        #region Private methods

        IEnumerator FinishDelay()
        {
            yield return new WaitForSeconds(DELAY_TIME);
            ScoreManager_OnScoreChanged(0);
            UIInitializer.GameOverScreenInstance.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        #endregion


        #region Event handlers

        void MenuButton_OnClick()
        {
            if (LevelInitializer.LevelManagerInstance.GameIsEnabled)
            {
                UIInitializer.MenuScreenInstance.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
        }


        void ScoreManager_OnScoreChanged(int newScore)
        {
            scoreText.text = newScore.ToString();
        }


        void LevelManager_OnFinish()
        {
            StartCoroutine(FinishDelay());
        }

        #endregion
    }
}
