﻿using UnityEngine;
using Level.Initializers;


namespace UI
{
    public class MenuScreen : MonoBehaviour
    {
        #region Fields

        [SerializeField] tk2dUIItem resetButton;
        [SerializeField] tk2dUIItem resumeButton;

        #endregion


        #region Unity lifecycle

        void OnEnable()
        {
            resetButton.OnClick += ResetButton_OnClick;
            resumeButton.OnClick += ResumeButton_OnClick;
            Time.timeScale = 0;
        }


        void OnDisable()
        {
            resetButton.OnClick -= ResetButton_OnClick;
            resumeButton.OnClick -= ResumeButton_OnClick;
            Time.timeScale = 1;
        }

        #endregion


        #region Private methods

        void SwitchToGameScreen()
        {
            UIInitializer.GameScreenInstance.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        #endregion


        #region Event handlers

        void ResumeButton_OnClick()
        {
            SwitchToGameScreen();
        }


        void ResetButton_OnClick()
        {
            SwitchToGameScreen();
            LevelInitializer.LevelManagerInstance.Reset();
        }

        #endregion
    }
}
