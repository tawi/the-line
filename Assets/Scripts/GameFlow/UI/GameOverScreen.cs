﻿using UnityEngine;
using Level.Initializers;


namespace UI
{
    public class GameOverScreen : MonoBehaviour
    {
        #region Fields

        [SerializeField] tk2dUIItem resetButton;
        [SerializeField] tk2dTextMesh totalScoreText;
        [SerializeField] tk2dTextMesh highScoreText;

        #endregion


        #region Unity lifecycle

        void OnEnable()
        {
            totalScoreText.text = GameInitializer.ScoreManagerInstance.Score.ToString();
            highScoreText.text = GameInitializer.ScoreManagerInstance.HighScore.ToString();
            resetButton.OnClick += ResetButton_OnClick;
        }


        void OnDisable()
        {
            resetButton.OnClick -= ResetButton_OnClick;
        }

        #endregion


        #region Event handlers

        void ResetButton_OnClick()
        {
            LevelInitializer.LevelManagerInstance.Reset();
            UIInitializer.GameScreenInstance.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        #endregion
    }
}
